Source: prody
Section: science
Priority: optional
Maintainer: Debichem Team <debichem-devel@lists.alioth.debian.org>
Uploaders:
 Andrius Merkys <merkys@debian.org>,
Rules-Requires-Root: no
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all-dev,
 python3-biopython <!nocheck>,
 python3-numpy,
 python3-pyparsing <!nocheck>,
 python3-scipy <!nocheck>,
 python3-setuptools,
 python3-zombie-imp <!nocheck>,
Standards-Version: 4.6.2
Homepage: https://github.com/prody/ProDy
Vcs-Browser: https://salsa.debian.org/debichem-team/prody
Vcs-Git: https://salsa.debian.org/debichem-team/prody.git
Testsuite: autopkgtest-pkg-pybuild

Package: python3-prody
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Provides:
 ${python3:Provides},
X-Python3-Version: ${python3:Versions}
Description: Python package for protein dynamics analysis
 ProDy is a free and open-source Python package for protein structure, dynamics,
 and sequence analysis. It allows for comparative analysis and modeling of
 protein structural dynamics and sequence co-evolution. Fast and flexible ProDy
 API is for interactive usage as well as application development. ProDy also
 comes with several analysis applications and a graphical user interface for
 visual analysis.

Package: python3-prody-tests
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python3-prody (<< 2.3.1),
Replaces:
 python3-prody (<< 2.3.1),
X-Python3-Version: ${python3:Versions}
Description: Python package for protein dynamics analysis - test data
 ProDy is a free and open-source Python package for protein structure, dynamics,
 and sequence analysis. It allows for comparative analysis and modeling of
 protein structural dynamics and sequence co-evolution. Fast and flexible ProDy
 API is for interactive usage as well as application development. ProDy also
 comes with several analysis applications and a graphical user interface for
 visual analysis.
 .
 This package contains the test data for python3-prody.
